/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GisService } from './gis.service';

describe('Service: Gis', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GisService]
    });
  });

  it('should ...', inject([GisService], (service: GisService) => {
    expect(service).toBeTruthy();
  }));
});
