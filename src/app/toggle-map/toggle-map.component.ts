import { Component, OnInit } from '@angular/core';
import { loadModules } from 'esri-loader';
import { GisService } from '../gis.service';

@Component({
	selector: 'app-toggle-map',
	templateUrl: './toggle-map.component.html',
	styleUrls: ['./toggle-map.component.scss']
})
export class ToggleMapComponent implements OnInit {

	showStyle = 'inline-block';
	hideStyle = 'none';

	divList: string;
	btnNostraStyle: string;
	btnSatelliteStyle: string;
	btnStreetStyle: string;
	btnTopoStyle: string;

	baseMapLayer: any;

	btnSelectClass: string;

	esriBaseMap: any;
	esriTileLayer: any;

	strConfigMap = {
		nostra: {
			name: 'nostra',
			url: 'https://onedpt.dpt.go.th/arcgis/rest/services/Nostra/MapServer'

		},
		street: {
			name: 'street',
			url: 'https://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer'
		},
		satellite: {
			name: 'satellite',
			url: 'https://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer'
		},
		topo: {
			name: 'topo',
			url: 'https://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer'
		}
	};

	constructor(private gisService: GisService) { }

	ngOnInit() {
		loadModules([
			'esri/Basemap',
			'esri/layers/TileLayer'
		]).then(([
			Basemap,
			TileLayer
		]) => {
			this.esriBaseMap = Basemap;
			this.esriTileLayer = TileLayer;
		});

		this.divList = this.hideStyle;
		this.btnNostraStyle = this.hideStyle;
		this.btnStreetStyle = this.showStyle;
		this.btnSatelliteStyle = this.showStyle;
		this.btnTopoStyle = this.showStyle;
		this.btnSelectClass = this.strConfigMap.nostra.name;
	}

	onOpenListClick() {
		if (this.divList != this.showStyle) {
			this.divList = this.showStyle;
		} else {
			this.divList = this.hideStyle;
		}
	}

	onToggleMapClick(name) {
		if (name == this.strConfigMap.nostra.name) {
			this.getNostra();
		}
		else if (name == this.strConfigMap.street.name) {
			this.getStreet();
		}
		else if (name == this.strConfigMap.satellite.name) {
			this.getSatellite();
		}
		else if (name == this.strConfigMap.topo.name) {
			this.getTopo();
		}
	}

	onMouseHold(name): string {
		let itemName;
		if (name === this.strConfigMap.nostra.name) {
			itemName = 'แผนที่ฐาน NOSTRA';
		} else if (name === this.strConfigMap.street.name) {
			itemName = 'แผนที่เชิงเส้นถนน';
		} else if (name === this.strConfigMap.satellite.name) {
			itemName = 'แผนที่ภาพถ่ายดาวเทียม';
		} else if (name === this.strConfigMap.topo.name) {
			itemName = 'แผนที่เฉดสีเทา';
		}
		return itemName;
	}

	createMap(url: string) {
		const basemap = new this.esriBaseMap({
			baseLayers: [
				new this.esriTileLayer({
					url: url,
					// token: this.appService.portalToken
				})
			]
		});
		return basemap;
	}

	getNostra() {
		this.gisService.map.basemap = this.createMap(this.strConfigMap.nostra.url);
		this.btnSelectClass = this.strConfigMap.nostra.name;

		this.btnNostraStyle = this.hideStyle;
		this.btnStreetStyle = this.showStyle;
		this.btnSatelliteStyle = this.showStyle;
		this.btnTopoStyle = this.showStyle;
		this.divList = this.hideStyle;
	}

	getStreet() {
		this.gisService.map.basemap = this.createMap(this.strConfigMap.street.url);
		this.btnSelectClass = this.strConfigMap.street.name;

		this.btnNostraStyle = this.showStyle;
		this.btnStreetStyle = this.hideStyle;
		this.btnSatelliteStyle = this.showStyle;
		this.btnTopoStyle = this.showStyle;
		this.divList = this.hideStyle;
	}

	getSatellite() {
		this.gisService.map.basemap = this.createMap(this.strConfigMap.satellite.url);
		this.btnSelectClass = this.strConfigMap.satellite.name;

		this.btnNostraStyle = this.showStyle;
		this.btnStreetStyle = this.showStyle;
		this.btnSatelliteStyle = this.hideStyle;
		this.btnTopoStyle = this.showStyle;
		this.divList = this.hideStyle;
	}

	getTopo() {
		this.gisService.map.basemap = this.createMap(this.strConfigMap.topo.url);
		this.btnSelectClass = this.strConfigMap.topo.name;

		this.btnNostraStyle = this.showStyle;
		this.btnStreetStyle = this.showStyle;
		this.btnSatelliteStyle = this.showStyle;
		this.btnTopoStyle = this.hideStyle;
		this.divList = this.hideStyle;
	}

}
