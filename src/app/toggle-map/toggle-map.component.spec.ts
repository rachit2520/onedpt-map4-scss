import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleMapComponent } from './toggle-map.component';

describe('ToggleMapComponent', () => {
  let component: ToggleMapComponent;
  let fixture: ComponentFixture<ToggleMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
