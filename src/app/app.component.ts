import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { GisService } from '../app/gis.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  @ViewChild("mapViewNode", { static: true }) private mapViewEl: ElementRef;
  attributesarray;
  title = 'onedpt-map4-scss';
  mappoint: any;
  loading: boolean = true;
  constructor(private gisService: GisService) {
    this.gisService.getMapPoint().subscribe((value) => {
      this.mappoint = value
    })

    this.gisService.getLoading().subscribe((value) => {
      console.log('Loading : ', value)
      this.loading = value
    })
  }

  ngOnInit() {


    this.gisService.loadMap(this.mapViewEl).then(attrarray => this.attributesarray = attrarray)

  } // ngOnInit

  gotoPoint(){
    this.gisService.gotoPoint("POINT(642475.400606103 1523422.858136008)");
  }

  cmt(){
    this.gisService.cmt();
  }

  res(){
    this.gisService.res();
  }

}
