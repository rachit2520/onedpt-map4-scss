import { ElementRef, Injectable, ViewChild } from "@angular/core";
import { loadModules } from "esri-loader";
import { Subject } from 'rxjs'

@Injectable({
  providedIn: "root",
})
export class GisService {
  view: any;
  view1: any;
  map: any;
  esriBaseMap: any;
  esriTileLayer: any;
  popupTemplate: any = {};
  layers: any;
  featureLayer1: any;

  pin_01 = {
    type: "picture-marker",
    url: "assets/images/pin_01.png",
    width: 20,
    height: 24,
    xoffset: 0,
    yoffset: 8
  };
  pin_02 = {
    type: "picture-marker",
    url: "assets/images/pin_02.png",
    width: 20,
    height: 24,
    xoffset: 0,
    yoffset: 8
  };
  pin_03 = {
    type: "picture-marker",
    url: "assets/images/pin_03.png",
    width: 20,
    height: 24,
    xoffset: 0,
    yoffset: 8
  };

  public MapPoint = new Subject<any>();
  public Loading = new Subject<any>();

  constructor() {
    this.Loading.next(true);
  }

  async loadMap(mapContainer: ElementRef) {
    const [Map] = await loadModules(['esri/Map']);
    const [MapView] = await loadModules(['esri/views/MapView']);
    const [Basemap] = await loadModules(['esri/Basemap']);
    const [TileLayer] = await loadModules(['esri/layers/TileLayer']);
    const [Fullscreen] = await loadModules(['esri/widgets/Fullscreen']);
    const [Locate] = await loadModules(['esri/widgets/Locate']);
    const [Compass] = await loadModules(['esri/widgets/Compass']);
    const [EsriUtils] = await loadModules(['esri/core/urlUtils']);
    const [Home] = await loadModules(['esri/widgets/Home']);
    const [projection] = await loadModules(['esri/geometry/projection']);

    await projection.load();

    EsriUtils.addProxyRule({
      // proxyUrl: 'https://onedptweb.cdg.co.th/onedpt-ws/api/appproxy',
      proxyUrl: 'https://onedptgis.dpt.go.th/onedpt-ws-non/api/appproxy',
      urlPrefix: 'https://onedpt.dpt.go.th/arcgis/rest'
    });

    this.esriBaseMap = Basemap;
    this.esriTileLayer = TileLayer;

    const basemap = new this.esriBaseMap({
      baseLayers: [
        new this.esriTileLayer({
          url: "https://onedpt.dpt.go.th/arcgis/rest/services/Nostra/MapServer",
          // token: this.appService.portalToken
        })
      ]
    });

    const mapProperties = {
      basemap: basemap
    };

    this.map = new Map(mapProperties);

    // Initialize the MapView
    const mapViewProperties = {
      container: mapContainer.nativeElement,
      map: this.map,
      extent: {
        // autocasts as new Extent()
        xmax: 13112025.728494663,
        xmin: 9354992.914222676,
        ymax: 2391261.7459102366,
        ymin: 534759.2029203695,
        spatialReference: 102100
      }

    };

    this.view = new MapView(mapViewProperties);

    var fullscreen = new Fullscreen({
      view: this.view
    });

    var locateBtn = new Locate({
      view: this.view
    });

    var compassWidget = new Compass({
      view: this.view
    });

    var homeBtn = new Home({
      view: this.view
    });

    this.view.ui.add(fullscreen, "bottom-left");
    this.view.ui.add(locateBtn, "top-left");
    this.view.ui.add(compassWidget, "bottom-left");
    this.view.ui.add(homeBtn, "top-left");

    this.view.on("click", (evt: any) => {
      this.view.hitTest(evt.screenPoint).then(async (response: any) => {
        if (response && response.results.length) {
          this.view.goTo(evt.mapPoint)
        } else {
          this.clearGraphic("mapLayer");
          this.gotoXY(evt.mapPoint.longitude, evt.mapPoint.latitude);
        }
      });

    });

    return this.view;

  }

  async cmt(){
    this.map.layers.removeAll();

    const [MapImageLayer] = await loadModules(['esri/layers/MapImageLayer']);
    this.layers = new MapImageLayer({
      url: "https://onedpt.dpt.go.th/arcgis/rest/services/TOWNPLAN/CPLLU_NON/MapServer"
    });
    await this.layers.load();
    const displayLayers = [0, 1, 4, 5, 6];
    this.layers.allSublayers.items.forEach(item => {
      if (!displayLayers.includes(item.id)) {
        item.set("listMode", "hide");
        item.set("visible", false);
      } else {
        item.set("visible", true);
      }
    });

    this.layers.opacity = 0.3;

    this.layers.on("layerview-create", () => {
      this.Loading.next(false);
    });

    this.view.map.add(this.layers);



    const [FeatureLayer] = await loadModules(['esri/layers/FeatureLayer']);
    const featureLayer = new FeatureLayer({
      url: "https://onedpt.dpt.go.th/arcgis/rest/services/TOWNPLAN/CPLLU_NON/MapServer/6",
    });
    this.view.map.add(featureLayer);
    this.view.whenLayerView(featureLayer).then(function(layerView) {
      layerView.watch("updating", function(val) {
      // wait for the layer view to finish updating
        if (!val) {
          layerView.queryExtent().then(async(response) => {
          // go to the extent of all the graphics in the layer view
          await this.view.goTo(response.extent);
          layerView.visible = false;
          });
        }
      });
    });

  }

  async res(){
    this.map.layers.removeAll();
    const [FeatureLayer] = await loadModules(['esri/layers/FeatureLayer']);
    const featureLayer = new FeatureLayer({
      url: "http://onedpt.dpt.go.th/arcgis/rest/services/COMPL/COMPL_MAIN/MapServer/0",
      // 0 - แสดงความคิดเห็น
      // 1 - สงวนสิทธิ์คำร้อง
      // 2 - ยื่นคำร้อง
      // 3 - ปิดประกาศ
      definitionExpression: "ANN_ID = 46"
    });
    await featureLayer.load();
    this.view.map.add(featureLayer);

  }

  async gotoPoint(pt: any){
    const [Point] = await loadModules(["esri/geometry/Point"]);
    const [Graphic] = await loadModules(["esri/Graphic"]);
    const [projection] = await loadModules(['esri/geometry/projection']);
    await projection.load();

    let _layer: any;
    this.clearGraphic("mapLayer");

    console.log("stGeo : ", pt);

    let pointString = pt.replace('POINT(', '');
    pointString = pointString.replace(')', '');
    const pointList = pointString.split(' ');

    var point = {
      type: "point",
      longitude: pointList[0],
      latitude: pointList[1],
      spatialReference: { wkid: 32647 }
    };

    let gSysmbol = this.getSymbol('3')

    this.createPopupTemplate("title", "data");

    let pointGraphic = new Graphic({
      geometry: point,
      // popupTemplate: this.popupTemplate,
      symbol: gSysmbol
    });

    let geometry = projection.project(pointGraphic.geometry, { wkid: 4326 });
    this.view.goTo(geometry);

    console.log("Lat : ", geometry.latitude);
    console.log("Lng : ", geometry.longitude);

    let gLayer = await this.isExistingLayer('mapLayer')
    if (!gLayer) {
      _layer = await this.createLayer('mapLayer');
    } else {
      _layer = gLayer;
    }
    if (_layer) {
      _layer.add(pointGraphic);
    }


  }

  getMapPoint() {
    return this.MapPoint.asObservable()
  }

  getLoading() {
    return this.Loading.asObservable()
  }


  async gotoXY(lng: any, lat: any){
    const [Point] = await loadModules(["esri/geometry/Point"]);
    const [Graphic] = await loadModules(["esri/Graphic"]);
    const [projection] = await loadModules(['esri/geometry/projection']);
    await projection.load();

    let _layer: any;

    this.clearGraphic("mapLayer");

    let geo = {
      x: lng,
      y: lat,
      spatialReference: {
        wkid: 4326
      }
    };

    let geometry = projection.project(geo, { wkid: 32647 });

    const stGeo = this.toST(geometry);  // convert to string

    console.log("lat : ", lat);
    console.log("lng : ", lng);
    console.log("stGeo : ", stGeo);

    this.MapPoint.next(stGeo)
    this.view.goTo({
      target: new Point(lng, lat),
    });

    var point = {
      type: "point",
      longitude: lng,
      latitude: lat,
      spatialReference: { wkid: 4326 }
    };

    let gSysmbol = this.getSymbol('1')

    // this.createPopupTemplate("title", "data");

    let pointGraphic = new Graphic({
      geometry: point,
      // popupTemplate: this.popupTemplate,
      symbol: gSysmbol
    });

    // var graphicsLayer = new GraphicsLayer({ id: 'mapLayer' });
    // graphicsLayer.add(pointGraphic);

    let gLayer = await this.isExistingLayer('mapLayer')
    if (!gLayer) {
      _layer = await this.createLayer('mapLayer')
    } else {
      _layer = gLayer
    }
    if (_layer) {
      _layer.add(pointGraphic)
    }

  }

  createPopupTemplate(title: any, data: any) {
    this.popupTemplate.title = title;
    this.popupTemplate.content = data;
    console.log('popupTemplate : ', this.popupTemplate);
  }

  public toST(geometry: any): string {
    let stringST: string = '';
    if (geometry.x && geometry.y) {
      stringST = `POINT(${geometry.x} ${geometry.y})`;
    } else if (geometry.points) {
      stringST = 'MULTIPOINT({0})'.replace('{0}', geometry.points.map(r => r.join(' ')).join(','));
    } else if (geometry.paths) {
      if (geometry.paths.length == 1) {
        stringST = 'LINESTRING{0}';
      } else if (geometry.paths.length > 1) {
        stringST = 'MULTILINESTRING({0})';
      }
      stringST = stringST.replace('{0}', geometry.paths.map(r => {
        return '({0})'.replace('{0}', r.map(p => {
          return p.join(' ');
        }).join(','));
      }).join(','));
    } else if (geometry.rings) {
      if (geometry.rings.length == 1) {
        stringST = 'POLYGON{0}';
      } else if (geometry.rings.length > 1) {
        stringST = 'MULTIPOLYGON({0})';
      }
      stringST = stringST.replace('{0}', geometry.rings.map(r => {
        return '(({0}))'.replace('{0}', r.map(p => {
          return p.join(' ');
        }).join(','));
      }).join(','));
    } else if (geometry.xmin && geometry.ymin) {
      stringST = 'POLYGON(({xmin} {ymax},{xmax} {ymax},{xmax} {ymin},{xmin} {ymin},{xmin} {ymax}))';
      stringST = stringST.replace(/{xmin}/g, geometry.xmin);
      stringST = stringST.replace(/{xmax}/g, geometry.xmax);
      stringST = stringST.replace(/{ymin}/g, geometry.ymin);
      stringST = stringST.replace(/{ymax}/g, geometry.ymax);
    }

    return stringST;
  }

  async createLayer(layerId: string) {
    const [GraphicsLayer] = await loadModules(['esri/layers/GraphicsLayer']);
    try {
      const layer = new GraphicsLayer({ id: layerId });
      // Add GraphicsLayer to map
      await this.view.map.add(layer);
      return layer || undefined;
    }
    catch (exception) {
      return undefined;
    }
  }

  async isExistingLayer(layerId: string) {
    try {
      const layer = this.view.map.findLayerById(layerId);
      return layer || undefined;
    }
    catch (exception) {
      return undefined;
    }
  }

  async clearGraphic(layerId) {
    let gLayer = await this.isExistingLayer(layerId)
    if (gLayer) {
      gLayer.removeAll();
    }
  }

  getSymbol(type): any {
    var pin
    switch (type) {
      case '1': {
        pin = this.pin_01;
        break
      }
      case '2': {
        pin = this.pin_02;
        break
      }
      case '3': {
        pin = this.pin_03;
        break
      }
      default: {
        pin = this.pin_01;
        break
      }
    }
    return pin
  }


}
